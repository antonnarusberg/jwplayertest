//
//  ViewController.swift
//  JWPlayerTest
//
//  Created by Anton Narusberg on 06/03/2017.
//  Copyright © 2017 Postimees. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var playerContainer: UIView!
    private var player: JWPlayerController?

    override func viewDidLoad() {
        super.viewDidLoad()

        loadVideo()
    }

    func loadVideo() {
        let config: JWConfig = JWConfig(contentURL: "http://postimees-vod.babahhcdn.com/bb1051/_definst_/smil:store/postimees/2017_03/2017_03_05_muu_piltseiliikluses_W9Tgg_1/play.smil/playlist.m3u8")
        config.premiumSkin = JWPremiumSkinGlow

        let prerollUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=650x366&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&vpos=preroll&cust_params=video%3D6317815%26article%3D4031907%26section%3D81&iu=%2F82349501%2F608%2F552&correlator=\(Int(NSDate().timeIntervalSince1970))"

        config.adSchedule = [JWAdBreak(tag: prerollUrl, offset:"pre")]

        self.player = JWPlayerController(config: config)

        self.player!.view.frame = self.playerContainer.bounds
        self.player!.view.autoresizingMask = [UIViewAutoresizing.flexibleBottomMargin, UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleLeftMargin, UIViewAutoresizing.flexibleRightMargin, UIViewAutoresizing.flexibleTopMargin, UIViewAutoresizing.flexibleWidth]

        self.playerContainer.addSubview(self.player!.view)
    }


    @IBAction func reloadVideo(_ sender: Any) {
        loadVideo()
    }

}

